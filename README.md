**Lafayette dentist office**

Our finest dentist office in Lafayette, Indiana, delivers comprehensive dental care to people of all ages at all five of 
our locations, from Bloomington, Illinois, to Fort Wayne, Indiana. 
Please Visit Our Website [Lafayette dentist office](https://dentistlafayetteindiana.com/dentist-office.php) for more information. 

---

## Our dentist office in Lafayette 

Our dental office in Lafayette Indiana is well prepared and proud to give your whole family a healthy, beautiful smile. 
Our best Lafayette Indiana dentist office and consultants will make the visit as easy and as painless and minimally 
invasive as possible, whether you're looking for regular care, a smile restoration, extensive restorative work, 
orthodontics or advanced facilities.
Not only do we want to make your smile bigger, but we want you to leave our office with one every time you visit. 
Three primary forms of dentistry are included in our wide range of facilities and capabilities: preventive, cosmetic and general.
